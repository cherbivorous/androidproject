package com.example.newsproject;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;

public class Main2Activity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main2);
		
		ImageButton btnThairath = (ImageButton) this.findViewById(R.id.btnThairath);
		ImageButton btnBkkpost = (ImageButton) this.findViewById(R.id.btnBkkpost);
		ImageButton btnBBC = (ImageButton) this.findViewById(R.id.btnBBC);
		ImageButton btnCNN = (ImageButton) this.findViewById(R.id.btnCNN);
		
		// Add event to component
		btnThairath.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				TRCategory();
			}
		});
				
		btnBkkpost.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				BPCategory();
			}
		});
		
		btnBBC.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				BBCCategory();
			}
		});
		
		btnCNN.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				CNNCategory();
			}
		});
	}

	private void TRCategory() {
		Intent intent = new Intent(this, TRCategoryActivity.class);
		startActivity(intent);
	}
	
	private void BPCategory() {
		Intent intent = new Intent(this, BPCategoryActivity.class);
		startActivity(intent);
	}
	
	private void BBCCategory() {
		Intent intent = new Intent(this, BBCCategoryActivity.class);
		startActivity(intent);
	}
	
	private void CNNCategory() {
		Intent intent = new Intent(this, CNNCategoryActivity.class);
		startActivity(intent);
	}
	
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main2, menu);
		return true;
	}

}
