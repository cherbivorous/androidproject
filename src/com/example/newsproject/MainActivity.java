package com.example.newsproject;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		ImageButton btnMain = (ImageButton) this.findViewById(R.id.btnMain);
		Button btnAbout = (Button) this.findViewById(R.id.btnAbout);
		
		btnMain.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Main();
			}
		});
		
		btnAbout.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				About();
			}
		});
	}
	
	private void Main() {
		Intent intent = new Intent(this, Main2Activity.class);
		startActivity(intent);
	}
	
	private void About() {
		Intent intent = new Intent(this, AboutActivity.class);
		startActivity(intent);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
