package com.example.newsproject;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class BBCCategoryActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_bbccategory);
		
		Button btnTopsBBC = (Button) this.findViewById(R.id.btnTopsBBC);
		Button btnWorldBBC = (Button) this.findViewById(R.id.btnWorldBBC);
		Button btnUK = (Button) this.findViewById(R.id.btnUK);
		Button btnBusinessBBC = (Button) this.findViewById(R.id.btnBusinessBBC);
		Button btnPoliticsBBC = (Button) this.findViewById(R.id.btnPoliticsBBC);
		Button btnHealthBBC = (Button) this.findViewById(R.id.btnHealthBBC);
		Button btnEduBBC = (Button) this.findViewById(R.id.btnEduBBC);
		Button btnSciBBC = (Button) this.findViewById(R.id.btnSciBBC);
		Button btnTechBBC = (Button) this.findViewById(R.id.btnTechBBC);
		
		// Add event to component
		btnTopsBBC.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Topstories();
			}
		});
		
		btnWorldBBC.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				World();
			}
		});
		
		btnUK.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				UK();
			}
		});
		
		btnBusinessBBC.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Business();
			}
		});
		
		btnPoliticsBBC.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Politics();
			}
		});

		btnHealthBBC.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Health();
			}
		});
		
		btnEduBBC.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Education();
			}
		});
		
		btnSciBBC.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Science();
			}
		});
		
		btnTechBBC.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Technology();
			}
		});
	}
	
	private void Topstories() {
		Intent intent = new Intent(this, HomeActivity.class);
		intent.putExtra("URL", "http://feeds.bbci.co.uk/news/rss.xml");
		startActivity(intent);
	}
	
	private void World() {
		Intent intent = new Intent(this, HomeActivity.class);
		intent.putExtra("URL", "http://feeds.bbci.co.uk/news/world/rss.xml");
		startActivity(intent);
	}
	
	private void UK() {
		Intent intent = new Intent(this, HomeActivity.class);
		intent.putExtra("URL", "http://feeds.bbci.co.uk/news/uk/rss.xml");
		startActivity(intent);
	}
	
	private void Business() {
		Intent intent = new Intent(this, HomeActivity.class);
		intent.putExtra("URL", "http://feeds.bbci.co.uk/news/business/rss.xml");
		startActivity(intent);
	}
	
	private void Politics() {
		Intent intent = new Intent(this, HomeActivity.class);
		intent.putExtra("URL", "http://feeds.bbci.co.uk/news/politics/rss.xml");
		startActivity(intent);
	}

	private void Health() {
		Intent intent = new Intent(this, HomeActivity.class);
		intent.putExtra("URL", "http://feeds.bbci.co.uk/news/health/rss.xml");
		startActivity(intent);
	}
	
	private void Education() {
		Intent intent = new Intent(this, HomeActivity.class);
		intent.putExtra("URL", "http://feeds.bbci.co.uk/news/education/rss.xml");
		startActivity(intent);
	}
	
	private void Science() {
		Intent intent = new Intent(this, HomeActivity.class);
		intent.putExtra("URL", "http://feeds.bbci.co.uk/news/science_and_environment/rss.xml");
		startActivity(intent);
	}
	
	private void Technology() {
		Intent intent = new Intent(this, HomeActivity.class);
		intent.putExtra("URL", "http://feeds.bbci.co.uk/news/technology/rss.xml");
		startActivity(intent);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.bpcategory, menu);
		return true;
	}

}

