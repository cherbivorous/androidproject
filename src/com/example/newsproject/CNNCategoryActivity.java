package com.example.newsproject;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class CNNCategoryActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_cnncategory);
		
		Button btnTopsCNN = (Button) this.findViewById(R.id.btnTopsCNN);
		Button btnWorldCNN = (Button) this.findViewById(R.id.btnWorldCNN);
		Button btnAfrica = (Button) this.findViewById(R.id.btnAfrica);
		Button btnAmericas = (Button) this.findViewById(R.id.btnAmericas);
		Button btnAsia = (Button) this.findViewById(R.id.btnAsia);
		Button btnEurope = (Button) this.findViewById(R.id.btnEurope);
		Button btnCNNTech = (Button) this.findViewById(R.id.btnCNNTech);
		Button btnSciSpa = (Button) this.findViewById(R.id.btnSciSpa);
		Button btnEntCNN = (Button) this.findViewById(R.id.btnEntCNN);
		
		// Add event to component
		btnTopsCNN.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Topstories();
			}
		});
		
		btnWorldCNN.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				World();
			}
		});
		
		btnAfrica.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Africa();
			}
		});
		
		btnAmericas.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Americas();
			}
		});
		
		btnAsia.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Asia();
			}
		});

		btnEurope.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Europe();
			}
		});
		
		btnCNNTech.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				CNNTech();
			}
		});
		
		btnSciSpa.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Science();
			}
		});
		
		btnEntCNN.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Entertainment();
			}
		});
	}
	
	private void Topstories() {
		Intent intent = new Intent(this, HomeActivity.class);
		intent.putExtra("URL", "http://rss.cnn.com/rss/edition.rss");
		startActivity(intent);
	}
	
	private void World() {
		Intent intent = new Intent(this, HomeActivity.class);
		intent.putExtra("URL", "http://rss.cnn.com/rss/edition_world.rss");
		startActivity(intent);
	}
	
	private void Africa() {
		Intent intent = new Intent(this, HomeActivity.class);
		intent.putExtra("URL", "http://rss.cnn.com/rss/edition_africa.rss");
		startActivity(intent);
	}
	
	private void Americas() {
		Intent intent = new Intent(this, HomeActivity.class);
		intent.putExtra("URL", "http://rss.cnn.com/rss/edition_americas.rss");
		startActivity(intent);
	}
	
	private void Asia() {
		Intent intent = new Intent(this, HomeActivity.class);
		intent.putExtra("URL", "http://rss.cnn.com/rss/edition_asia.rss");
		startActivity(intent);
	}

	private void Europe() {
		Intent intent = new Intent(this, HomeActivity.class);
		intent.putExtra("URL", "http://rss.cnn.com/rss/edition_europe.rss");
		startActivity(intent);
	}
	
	private void CNNTech() {
		Intent intent = new Intent(this, HomeActivity.class);
		intent.putExtra("URL", "http://rss.cnn.com/rss/edition_technology.rss");
		startActivity(intent);
	}
	
	private void Science() {
		Intent intent = new Intent(this, HomeActivity.class);
		intent.putExtra("URL", "http://rss.cnn.com/rss/edition_space.rss");
		startActivity(intent);
	}
	
	private void Entertainment() {
		Intent intent = new Intent(this, HomeActivity.class);
		intent.putExtra("URL", "http://rss.cnn.com/rss/edition_entertainment.rss");
		startActivity(intent);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.cnncategory, menu);
		return true;
	}

}

