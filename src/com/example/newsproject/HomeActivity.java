package com.example.newsproject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import org.xmlpull.v1.XmlPullParserException;

import com.example.newsproject.MyParserEngine.NewsItem;

import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class HomeActivity extends ListActivity {
	
	private ArrayAdapter<NewsItem> adapter;
	private String url;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//setContentView(R.layout.activity_main);
		
		adapter = new ArrayAdapter<NewsItem> (this, android.R.layout.simple_list_item_1, android.R.id.text1);
		setListAdapter(adapter);

		DownloadFeedTask task = new DownloadFeedTask();
		//task.execute("http://www.thairath.co.th/rss/pol.xml");
		Intent data = this.getIntent();
		this.url = data.getStringExtra("URL");
		task.execute(this.url);
	}
	
	private InputStream downloadFeed(String strUrl) {
		try {
			URL url = new URL(strUrl);
			HttpURLConnection con = (HttpURLConnection)url.openConnection();
			return con.getInputStream();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	private class DownloadFeedTask extends AsyncTask<String, Void, ArrayList<NewsItem>> {
		
		protected ArrayList<NewsItem> doInBackground(String... urls) {
			InputStream stream = downloadFeed(urls[0]);
			
			try {
				return MyParserEngine.parse(stream);
			} catch (XmlPullParserException e) {
				e.printStackTrace();
				return null;
			} catch (IOException e) {
				e.printStackTrace();
				return null;
			}
		}
			
		protected void onPostExecute(ArrayList<NewsItem> items) {
			adapter.clear();
			for (NewsItem item : items) {
				adapter.add(item);
			}
			adapter.notifyDataSetChanged();
		}
	}

	protected void onListItemClick(ListView l, View v, int position, long id) {
		NewsItem item = (NewsItem)l.getItemAtPosition(position);
		String url = item.link;
		Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
		
		startActivity(i);
	}

}
