package com.example.newsproject;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class BPCategoryActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_bpcategory);
		
		Button btnBNews = (Button) this.findViewById(R.id.btnBNews);
		Button btnTops = (Button) this.findViewById(R.id.btnTopsBBC);
		Button btnNews = (Button) this.findViewById(R.id.btnMain);
		Button btnBusiness = (Button) this.findViewById(R.id.btnBusiness);
		Button btnLearning = (Button) this.findViewById(R.id.btnLearning);
		Button btnTech = (Button) this.findViewById(R.id.btnTech);
		Button btnLife = (Button) this.findViewById(R.id.btnLife);
		Button btnTravel = (Button) this.findViewById(R.id.btnTravel);
		
		// Add event to component
		btnBNews.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Breakingnews();
			}
		});
		
		btnTops.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Topstories();
			}
		});
		
		btnNews.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				News();
			}
		});
		
		btnBusiness.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Business();
			}
		});
		
		btnLearning.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Learning();
			}
		});
		
		btnTech.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Tech();
			}
		});
		
		btnLife.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Life();
			}
		});
		
		btnTravel.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Travel();
			}
		});
	}
	
	private void Breakingnews() {
		Intent intent = new Intent(this, HomeActivity.class);
		intent.putExtra("URL", "http://www.bangkokpost.com/rss/data/breakingnews.xml");
		startActivity(intent);
	}
	
	private void Topstories() {
		Intent intent = new Intent(this, HomeActivity.class);
		intent.putExtra("URL", "http://www.bangkokpost.com/rss/data/topstories.xml");
		startActivity(intent);
	}
	
	private void News() {
		Intent intent = new Intent(this, HomeActivity.class);
		intent.putExtra("URL", "http://www.bangkokpost.com/rss/data/news.xml");
		startActivity(intent);
	}
	
	private void Business() {
		Intent intent = new Intent(this, HomeActivity.class);
		intent.putExtra("URL", "http://www.bangkokpost.com/rss/data/business.xml");
		startActivity(intent);
	}
	
	private void Learning() {
		Intent intent = new Intent(this, HomeActivity.class);
		intent.putExtra("URL", "http://www.bangkokpost.com/rss/data/learning.xml");
		startActivity(intent);
	}
	
	private void Tech() {
		Intent intent = new Intent(this, HomeActivity.class);
		intent.putExtra("URL", "http://www.bangkokpost.com/rss/data/tect.xml");
		startActivity(intent);
	}
	
	private void Life() {
		Intent intent = new Intent(this, HomeActivity.class);
		intent.putExtra("URL", "http://www.bangkokpost.com/rss/data/life.xml");
		startActivity(intent);
	}
	
	private void Travel() {
		Intent intent = new Intent(this, HomeActivity.class);
		intent.putExtra("URL", "http://www.bangkokpost.com/rss/data/travel.xml");
		startActivity(intent);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.bpcategory, menu);
		return true;
	}

}
