package com.example.newsproject;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class TRCategoryActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_trcategory);
		
		Button btnPolitic = (Button) this.findViewById(R.id.btnTRPolitic);
		Button btnSport = (Button) this.findViewById(R.id.btnTRSport);
		Button btnTREnt = (Button) this.findViewById(R.id.btnTREnt);
		Button btnTRLife = (Button) this.findViewById(R.id.btnTRLife);
		Button btnTRTech = (Button) this.findViewById(R.id.btnTRTech);
		Button btnTREco = (Button) this.findViewById(R.id.btnTREco);
		Button btnTREdu = (Button) this.findViewById(R.id.btnTREdu);
		Button btnTROversea = (Button) this.findViewById(R.id.btnTROversea);
		
		// Add event to component
		btnPolitic.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				TRPolitic();
			}
		});
				
		btnSport.setOnClickListener(new OnClickListener() {
		    public void onClick(View v) {
		    	TRSport();
			}
		});
		
		btnTREnt.setOnClickListener(new OnClickListener() {
		    public void onClick(View v) {
		    	TREnt();
			}
		});
		
		btnTRLife.setOnClickListener(new OnClickListener() {
		    public void onClick(View v) {
		    	TRLife();
			}
		});
		
		btnTRTech.setOnClickListener(new OnClickListener() {
		    public void onClick(View v) {
		    	TRTech();
			}
		});
		
		btnTREco.setOnClickListener(new OnClickListener() {
		    public void onClick(View v) {
		    	TREco();
			}
		});
		
		btnTREdu.setOnClickListener(new OnClickListener() {
		    public void onClick(View v) {
		    	TREdu();
			}
		});
		
		btnTROversea.setOnClickListener(new OnClickListener() {
		    public void onClick(View v) {
		    	TROversea();
			}
		});
	}

	private void TRPolitic() {
		Intent intent = new Intent(this, HomeActivity.class);
		intent.putExtra("URL", "http://www.thairath.co.th/rss/pol.xml");
		startActivity(intent);
	}
	
	private void TRSport() {
		Intent intent = new Intent(this, HomeActivity.class);
		intent.putExtra("URL", "http://www.thairath.co.th/rss/sport.xml");
		startActivity(intent);
	}
	
	private void TREnt() {
		Intent intent = new Intent(this, HomeActivity.class);
		intent.putExtra("URL", "http://www.thairath.co.th/rss/ent.xml");
		startActivity(intent);
	}
	
	private void TRLife() {
		Intent intent = new Intent(this, HomeActivity.class);
		intent.putExtra("URL", "http://www.thairath.co.th/rss/life.xml");
		startActivity(intent);
	}
	
	private void TRTech() {
		Intent intent = new Intent(this, HomeActivity.class);
		intent.putExtra("URL", "http://www.thairath.co.th/rss/tech.xml");
		startActivity(intent);
	}
	
	private void TREco() {
		Intent intent = new Intent(this, HomeActivity.class);
		intent.putExtra("URL", "http://www.thairath.co.th/rss/eco.xml");
		startActivity(intent);
	}
	
	private void TREdu() {
		Intent intent = new Intent(this, HomeActivity.class);
		intent.putExtra("URL", "http://www.thairath.co.th/rss/edu.xml");
		startActivity(intent);
	}
	
	private void TROversea() {
		Intent intent = new Intent(this, HomeActivity.class);
		intent.putExtra("URL", "http://www.thairath.co.th/rss/oversea.xml");
		startActivity(intent);
	}
	
	
	@Override
 	public boolean onCreateOptionsMenu(Menu menu) {
 		getMenuInflater().inflate(R.menu.main, menu);

 		return true;
 	}
 

}